package com.ssrs.platform.code;

import com.ssrs.platform.FixedCodeType;
import com.ssrs.platform.util.PlatformUtil;

/**
 * 是或否
 *
 * @author ssrs
 */
public class YesOrNoNumber extends FixedCodeType {
	public static final String CODETYPE = "YesOrNoNumber";

	public static final String Yes = "1";
	public static final String No = "0";

	public YesOrNoNumber() {
		super(CODETYPE, "是或否(数字)", false, false);
		addFixedItem(Yes, "是", null);
		addFixedItem(No, "否", null);
	}

	public static boolean isYes(String str) {
		return Yes.equals(str);
	}

	public static boolean isNo(String str) {
		return !isYes(str);
	}

	public static String getName(String code) {
		return PlatformUtil.getCodeMap(CODETYPE).getStr(code);
	}
}
