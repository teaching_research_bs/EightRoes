package com.ssrs.platform.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author ssrs
 * @since 2020-11-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_message")
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 消息类型
     */
    private String type;

    /**
     * 消息子类型
     */
    private String subType;

    /**
     * 发送用户
     */
    private String formUser;

    /**
     * 接收用户
     */
    private String toUser;

    /**
     * 信箱
     */
    private String box;

    /**
     * 是否阅读
     */
    private Integer readFlag;

    /**
     * 是否弹出显示
     */
    private Integer popFlag;

    /**
     * 是否被发送者删除
     */
    private Integer delByFromUser;

    /**
     * 是否被接收者删除
     */
    private Integer delByToUser;

    /**
     * 主题
     */
    private String subject;

    /**
     * 内容
     */
    private String content;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    public static final String ID = "id";

    public static final String TYPE = "type";

    public static final String SUB_TYPE = "sub_type";

    public static final String FORM_USER = "form_user";

    public static final String TO_USER = "to_user";

    public static final String BOX = "box";

    public static final String READ_FLAG = "read_flag";

    public static final String POP_FLAG = "pop_flag";

    public static final String DEL_BY_FROM_USER = "del_by_from_user";

    public static final String DEL_BY_TO_USER = "del_by_to_user";

    public static final String SUBJECT = "subject";

    public static final String CONTENT = "content";

    public static final String CREATE_TIME = "create_time";

}
