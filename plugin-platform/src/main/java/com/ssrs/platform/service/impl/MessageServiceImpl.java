package com.ssrs.platform.service.impl;

import com.ssrs.platform.model.entity.Message;
import com.ssrs.platform.mapper.MessageMapper;
import com.ssrs.platform.service.IMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ssrs
 * @since 2020-11-15
 */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements IMessageService {

}
