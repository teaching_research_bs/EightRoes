package com.ssrs.platform.service;

import com.ssrs.platform.model.entity.Message;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ssrs
 * @since 2020-11-15
 */
public interface IMessageService extends IService<Message> {

}
