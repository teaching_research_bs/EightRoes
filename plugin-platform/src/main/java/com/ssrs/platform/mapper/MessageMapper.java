package com.ssrs.platform.mapper;

import com.ssrs.platform.model.entity.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ssrs
 * @since 2020-11-15
 */
public interface MessageMapper extends BaseMapper<Message> {

}
