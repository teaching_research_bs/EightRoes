package com.ssrs.platform.controller;


import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ssrs.framework.User;
import com.ssrs.framework.security.annotation.Priv;
import com.ssrs.framework.web.ApiResponses;
import com.ssrs.framework.web.BaseController;
import com.ssrs.platform.code.YesOrNo;
import com.ssrs.platform.code.YesOrNoNumber;
import com.ssrs.platform.model.entity.Message;
import com.ssrs.platform.service.IMessageService;
import com.ssrs.platform.util.Page;
import com.ssrs.platform.util.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ssrs
 * @since 2020-11-15
 */
@RestController
@RequestMapping("/api/message")
public class MessageController extends BaseController {

    @Autowired
    private IMessageService messageService;

    /**
     * 获取未读消息数量
     *
     * @return
     */
    @GetMapping("/count")
    @Priv
    public ApiResponses<Integer> count() {
        int count = messageService.count(Wrappers.<Message>lambdaQuery()
                .eq(Message::getToUser, User.getUserName())
                .eq(Message::getReadFlag, 0)
                .eq(Message::getDelByToUser, 0)
                .eq(Message::getDelByFromUser, 0));
        return success(count);
    }


    @GetMapping
    @Priv
    public ApiResponses<Page> list(@RequestParam Map<String, Object> paramas) {
        IPage<Message> ipage = messageService.page(new Query<Message>().getPage(paramas), Wrappers.<Message>lambdaQuery()
                .eq(Message::getToUser, User.getUserName())
                .eq(Message::getReadFlag, Convert.toInt(YesOrNoNumber.No))
                .eq(Message::getDelByToUser, Convert.toInt(YesOrNoNumber.No))
                .eq(Message::getDelByFromUser, Convert.toInt(YesOrNoNumber.No)));

        Page page = new Page(ipage);
        return success(page);
    }

}